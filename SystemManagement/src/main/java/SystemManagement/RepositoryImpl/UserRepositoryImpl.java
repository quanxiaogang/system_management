/**
 * 
 */
package SystemManagement.RepositoryImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import SystemManagement.Model.User;
import SystemManagement.Repository.UserRepository;

/**
 * @author linyuebin
 *
 */
public abstract class UserRepositoryImpl implements UserRepository {

	/**
	 * Mongodb数据操作工具类..
	 */
	@Autowired
	MongoOperations operations;

	public User getUserByAccount(String account) {
		Criteria criteria = new Criteria();
		Criteria.where("account").is(account);
		Query query = new Query();
		query.addCriteria(criteria);
		User user= operations.findOne(query, User.class);
		return user;
	}
}
