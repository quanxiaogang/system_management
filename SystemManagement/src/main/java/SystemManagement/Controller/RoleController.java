
/**
 * user
 */
package SystemManagement.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SystemManagement.Model.Role;
import SystemManagement.Repository.RoleRepository;

/**
 * @author linyuebin
 *
 */
@RestController
@RequestMapping("/role")
public class RoleController {
 
	@Autowired
	private RoleRepository roleRepository;

	@RequestMapping("/getRole")
    public Role getRole(@RequestParam(value="id") String id) {
		Role role=roleRepository.findOne(id);
	  return role;
    }

	@RequestMapping("/getRoleList")
	public List<Role> getRoleList() {
		return roleRepository.findAll();
	}

	@RequestMapping("/saveRole")
	public Role saveRole(@RequestBody Role role) {
		return roleRepository.save(role);
	}
}
