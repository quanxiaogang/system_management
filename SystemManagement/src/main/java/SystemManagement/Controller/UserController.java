
/**
 * user
 */
package SystemManagement.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SystemManagement.Model.User;
import SystemManagement.Repository.UserRepository;

/**
 * @author linyuebin
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@RequestMapping("/getUser")
    public User getUser(@RequestParam(value="id") String id) {
		User user= userRepository.findOne(id);
	  return user;
    }
	
	@RequestMapping("/getUserList")
    public List<User> getUserList() {
        return userRepository.findAll();
    }
	
 
	@RequestMapping("/saveUser")
    public User saveUser(@RequestBody  User user ) {
		 
        return userRepository.save(user);
    }
}
