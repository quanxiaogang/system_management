/**
 * 
 */
package SystemManagement.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import SystemManagement.Model.Menu;
import SystemManagement.Repository.MenuRepository;

/**
 * @author linyuebin
 * @date 2018年5月23日
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
	@Autowired
	private MenuRepository menuRepository;
	
	@RequestMapping("/getMenus")
	public List<Menu> getMenus(){
		return menuRepository.findAll();
	}
	@RequestMapping("/save")
	public void save(Menu entity) {
		menuRepository.save(entity);
	}
	@RequestMapping("/delete")
	public void delete(String id) {
		menuRepository.delete(id);
	}
	

}
