/**
 * 
 */
package SystemManagement.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SystemManagement.Model.User;
import SystemManagement.Repository.UserRepository;
import SystemManagement.Vo.ResponseResult;

/**
 * @author linyuebin
 *
 */
@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	UserRepository userRepository;

	/**
	 * 用户登录
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	@RequestMapping(value = ("/userLogin"), method = RequestMethod.GET)
	public ResponseResult<User> userLogin(@RequestParam(value = "account") String account,
			@RequestParam(value = "password") String password, HttpServletRequest request) throws Exception {
		ResponseResult<User> responseResult = new ResponseResult<User>();
		User user = userRepository.getUserByAccount(account);
		if (user == null) {
			responseResult.setSuccess(false);
			responseResult.setMessage("用户不存在！");
			return responseResult;
		}
		if (StringUtils.isEmpty(password)) {
			responseResult.setSuccess(false);
			responseResult.setMessage("密码错误");
		}
		if (password.equals(user.getPassword())) {
			responseResult.setSuccess(true);
			responseResult.setData(user);
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
		}

		return responseResult;
	}

	/**管理员登录
	 * @param account
	 * @param password
	 * @return
	 */
	@RequestMapping("/adminLogin")
	public boolean adminLogin(String account, String password) {

		User user = userRepository.getUserByAccount(account);
		if (user != null) {
			if (StringUtils.isEmpty(password)) {
				return false;
			}
			if (password.equals(user.getPassword())) {
				return true;
			}
		}
		return false;
	}

}
