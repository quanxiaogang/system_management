/**
 * 
 */
package SystemManagement.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import SystemManagement.Model.Admin;
import SystemManagement.Repository.AdminRepository;

/**
 * @author linyuebin
 *
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminRepository adminRepository;
	
	@RequestMapping(value= {"/createAdmin"},method= {RequestMethod.POST})
	public boolean createAdmin(Admin admin) {
		adminRepository.save(admin);
		return true;
	}
	
	@RequestMapping("/getAdminList")
	public List<Admin> getAdminList(){
		return adminRepository.findAll();
	}
	
	@RequestMapping("/saveAdmin")
	public boolean saveAdmin(Admin admin) {
		 adminRepository.save(admin);
		 return true;
	}
}
