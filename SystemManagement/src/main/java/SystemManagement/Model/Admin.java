package SystemManagement.Model;
 
public class Admin extends BaseModel{

    private String name;
    
    private String account;
    private String password;

    public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

    public String getUsername() {
        return name;
    }

    public void setUsername(String username) {
        this.name = username;
    }


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
