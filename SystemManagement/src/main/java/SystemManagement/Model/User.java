package SystemManagement.Model;
 
public class User extends BaseModel{

    private String userName;
 
    private String account;
    private String password;
    /**
     * 手机号码
     */
    private String mobile;

    public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
 
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
