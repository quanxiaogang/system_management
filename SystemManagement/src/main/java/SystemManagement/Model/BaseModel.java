/**
 * 
 */
package SystemManagement.Model;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;

/**
 * @author linyuebin
 *
 */
public class BaseModel {
	@Id
	private String id;
	
	private Date createDate;

	public BaseModel() {
    	if(this.id==null) {
    		this.id=UUID.randomUUID().toString();
    		this.createDate=new Date();
    	}
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
