/**
 * 
 */
package SystemManagement.Model;

/**
 * 菜单
 * 
 * @author linyuebin
 * @date 2018年5月21日
 */
public class Menu extends BaseModel {
	private String menuName;
	
	/**
	 * 等级 
	 */
	private int level;
	/**
	 * 上一级
	 */
	private int pLevel;
	/**
	 * 路由
	 */
	private String route; 
	
	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}

	public int getpLevel() {
		return pLevel;
	}

	public void setpLevel(int pLevel) {
		this.pLevel = pLevel;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}
}
