/**
 * 
 */
package SystemManagement.Model;

import java.util.Date;

/**再加燃料；再加油
 * @author linyuebin
 * @date 2018年4月16日
 */
public class Refueling extends BaseModel{
	
	private Date refuelingDate;
	private float totalAmount ;
	private String userId;
	public Date getRefuelingDate() {
		return refuelingDate;
	}
	public void setRefuelingDate(Date refuelingDate) {
		this.refuelingDate = refuelingDate;
	}
	public float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
