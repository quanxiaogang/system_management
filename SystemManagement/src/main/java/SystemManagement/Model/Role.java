package SystemManagement.Model;
/**
 * 角色
 * @author linyuebin
 * @date 2018年5月21日
 */
public class Role extends BaseModel{
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
