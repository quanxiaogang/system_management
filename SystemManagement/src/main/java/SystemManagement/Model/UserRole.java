/**
 * 
 */
package SystemManagement.Model;

/**
 * @author linyuebin
 * @date 2018年5月10日
 */
public class UserRole extends BaseModel{
	private String userId;
	private String roleId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
}
