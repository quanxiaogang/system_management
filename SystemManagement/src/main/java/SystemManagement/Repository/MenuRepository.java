package SystemManagement.Repository;
 

import org.springframework.data.mongodb.repository.MongoRepository;

import SystemManagement.Model.Menu;

 

 
public interface MenuRepository extends  MongoRepository<Menu, String>  {

    

}
