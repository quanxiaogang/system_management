package SystemManagement.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import SystemManagement.Model.Role;

/**
 * @author 程序猿DD
 * @version 1.0.0
 * @date 16/4/27 下午10:16.
 * @blog http://blog.didispace.com
 */
public interface RoleRepository extends  MongoRepository<Role, String>  {
    

}
