/*
 * Copyright 2000-2020 YGSoft.Inc All Rights Reserved.
 */
package SystemManagement.Repository;

import java.lang.reflect.Field;

import org.springframework.data.annotation.Id;

/**
 * 类的字段合并工具类.. <br>
 * @author chenxiangbai <br>
 * @version 1.0.0 2012-8-29 上午9:05:36 <br>
 * @see
 * @since JDK 1.7.0
 */
public final class ClassAnnotationUtils {

	/**
	 * 私有构造函数. <br>
	 * @author chenxiangbai 2012-8-29 上午9:06:09 <br>
	 */
	private ClassAnnotationUtils() {
	}
	
//	/**
//	 * 获取PO类注解的数据表名称.. <br>
//	 * @author chenxiangbai 2012-9-27 下午4:31:33 <br> 
//	 * @param boClass bo类型.
//	 * @return 数据表名称.
//	 */
//	public static String getTableName(final Class< ? > boClass) {
//		if (boClass.isAnnotationPresent(Table.class)) {
//			final Table table = (Table) boClass.getAnnotation(Table.class);
//			return table.name();
//		} else {
//			throw new RuntimeException(boClass.toString() + "没有设置@Table");
//		}
//	}
	
	/**
	 * 获取PO的主键属性.. <br>
	 * @author chenxiangbai 2012-9-27 下午4:31:33 <br> 
	 * @param boClass bo类型.
	 * @return 主键.
	 */
	public static Field getPrimaryKeyField(final Class< ? > boClass) {
		return getPrimaryKeyField(boClass.getSuperclass().getDeclaredFields());
	}
	
	/**
	 * 从字段列表中获取主键属性. <br>
	 * @author chenxiangbai 2012-9-27 下午4:31:33 <br> 
	 * @param fieldList 字段列表.
	 * @return 主键.
	 */
	public static Field getPrimaryKeyField(final Field[] fieldList) {
		Field primaryKeyField = null;
		for (Field field : fieldList) {
			if (field.isAnnotationPresent(Id.class)) {
				primaryKeyField = field;
				break;
			}
		}
		
		if (primaryKeyField != null) {
			return primaryKeyField;
		} else {
			throw new RuntimeException("没有找到主键属性");
		}
	}
	
//	/**
//	 * 获取PO的主键属性的注解数据列的名称. <br>
//	 * @author chenxiangbai 2012-9-27 下午4:32:26 <br> 
//	 * @param boClass  bo类型.
//	 * @return 数据列的名称.
//	 */
//	public static String getPrimaryKeyColumn(final Class< ? > boClass) {
//		final Field field = getPrimaryKeyField(boClass);
//		return getPrimaryKeyColumn(field);
//	}
	
//	/**
//	 * 从字段列表中获取主键属性的注解数据列的名称.. <br>
//	 * @author chenxiangbai 2012-9-27 下午4:32:26 <br> 
//	 * @param fieldList 字段列表.
//	 * @return 数据列的名称.
//	 */
//	public static String getPrimaryKeyColumn(final List<Field> fieldList) {
//		final Field field = getPrimaryKeyField(fieldList);
//		return getPrimaryKeyColumn(field);
//	}
	
//	/**
//	 * 获取属性的注解数据列的名称.. <br>
//	 * @author chenxiangbai 2012-9-27 下午4:32:26 <br> 
//	 * @param field 属性.
//	 * @return 数据列的名称.
//	 */
//	public static String getPrimaryKeyColumn(final Field field) {
//		final Column column = (Column) field.getAnnotation(Column.class);
//		if (column != null) {
//			return column.name();
//		} else {
//			throw new RuntimeException("没有设置@Id");
//		}
//	}
//
//	/**
//	 * 合并Field数组并去重，并实现过滤掉非Column字段，和实现Id放在首字段位置功能.. <br>
//	 * @author chenxiangbai 2012-8-29 上午8:59:49 <br>
//	 * @param fields1 Field数组1.
//	 * @param fields2 Field数组2.
//	 * @return 合并后的数组.
//	 */
//	public static List<Field> joinFields(final Field[] fields1, final Field[] fields2) {
//		final Map<String, Field> map = new LinkedHashMap<String, Field>();
//		for (Field field : fields1) {
//			// 过滤掉非Column定义的字段
//			if (!field.isAnnotationPresent(Column.class)) {
//				continue;
//			}
//			final Column column = (Column) field.getAnnotation(Column.class);
//			map.put(column.name(), field);
//		}
//
//		for (Field field : fields2) {
//			// 过滤掉非Column定义的字段
//			if (!field.isAnnotationPresent(Column.class)) {
//				continue;
//			}
//			final Column column = (Column) field.getAnnotation(Column.class);
//			if (!map.containsKey(column.name())) {
//				map.put(column.name(), field);
//			}
//		}
//
//		final List<Field> list = new ArrayList<Field>();
//		for (String key : map.keySet()) {
//			final Field tempField = map.get(key);
//			// 如果是Id则放在首位置.
//			if (tempField.isAnnotationPresent(Id.class)) {
//				list.add(0, tempField);
//			} else {
//				list.add(tempField);
//			}
//		}
//
//		return list;
//	}
	
	/**
	 * 获取PO对象的主键属性的值.. <br>
	 * @author chenxiangbai 2012-8-30 下午3:57:08 <br>
	 * @param cls 对象类型.
	 * @param <T> 对象类型.
	 * @param bo 业务对象.
	 * @return PO对象的主键属性的值.
	 */
	public static <T> String getKeyValue(final Class<T> cls, final T bo) {
		try {
			final Field primaryKeyField = ClassAnnotationUtils.getPrimaryKeyField(cls);
			primaryKeyField.setAccessible(true);
			return String.valueOf(primaryKeyField.get(bo));
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 获取PO对象的主键属性的值.. <br>
	 * @author chenxiangbai 2012-8-30 下午3:57:08 <br>
	 * @param cls 对象类型.
	 * @param <T> 对象类型.
	 * @param bo 业务对象.
	 * @return PO对象的主键属性的值.
	 */
	public static <T> Object getFieldValue(final Class<T> cls, final T bo, final String fieldName) {
		try {
			final Field primaryKeyField = ClassAnnotationUtils.getPrimaryKeyField(cls);
			primaryKeyField.setAccessible(true);
			return primaryKeyField.get(bo);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
